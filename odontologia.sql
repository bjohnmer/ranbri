-- phpMyAdmin SQL Dump
-- version 4.0.6deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 12, 2014 at 02:42 PM
-- Server version: 5.5.34-0ubuntu0.13.10.1
-- PHP Version: 5.5.3-1ubuntu2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `odontologia`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `especialidades`
--

CREATE TABLE IF NOT EXISTS `especialidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(75) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `especialidades`
--

INSERT INTO `especialidades` (`id`, `nombre`) VALUES
(3, 'Odontología');

-- --------------------------------------------------------

--
-- Table structure for table `galerias`
--

CREATE TABLE IF NOT EXISTS `galerias` (
  `galeria_id` int(11) NOT NULL AUTO_INCREMENT,
  `galeria_img` varchar(255) NOT NULL,
  `galeria_fecha` date NOT NULL,
  `historia_id` int(11) NOT NULL,
  PRIMARY KEY (`galeria_id`),
  KEY `historia_id` (`historia_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `historias`
--

CREATE TABLE IF NOT EXISTS `historias` (
  `historia_id` int(11) NOT NULL AUTO_INCREMENT,
  `historia_num` varchar(20) NOT NULL,
  `paciente_id` int(11) NOT NULL,
  `historia_mc` text COMMENT 'Motivo de Consulta',
  `historia_tp` text COMMENT 'Tratamiento Previo',
  `historia_alergico` text,
  `historia_habitos` text,
  `historia_am` text COMMENT 'Antecedentes Médicos',
  `historia_soportes` text,
  `historia_maxsup` text,
  `historia_maxinf` text,
  `historia_oclusion` text,
  `historia_fecha` date NOT NULL,
  PRIMARY KEY (`historia_id`),
  KEY `paciente_id` (`paciente_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `medicos`
--

CREATE TABLE IF NOT EXISTS `medicos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cedula` int(11) NOT NULL,
  `matricula` varchar(10) NOT NULL,
  `nombres` varchar(100) NOT NULL,
  `especialidad_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cedula` (`cedula`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `medicos`
--

INSERT INTO `medicos` (`id`, `cedula`, `matricula`, `nombres`, `especialidad_id`) VALUES
(1, 42134231, '423413432', 'Juan González', 3),
(2, 42443232, '76543234', 'Carlos Pérez', 3);

-- --------------------------------------------------------

--
-- Table structure for table `odontodiagrama`
--

CREATE TABLE IF NOT EXISTS `odontodiagrama` (
  `odontodiagrama_id` int(11) NOT NULL AUTO_INCREMENT,
  `odontodiagrama_pieza` int(11) NOT NULL,
  `historia_id` int(11) NOT NULL,
  PRIMARY KEY (`odontodiagrama_id`),
  KEY `historia_id` (`historia_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pacientes`
--

CREATE TABLE IF NOT EXISTS `pacientes` (
  `paciente_id` int(11) NOT NULL AUTO_INCREMENT,
  `paciente_nombrecompleto` varchar(100) NOT NULL,
  `paciente_ci` int(11) NOT NULL,
  `paciente_edad` int(11) DEFAULT NULL,
  `paciente_fnac` date DEFAULT NULL,
  `paciente_sexo` enum('Masculino','Femenino') NOT NULL DEFAULT 'Masculino',
  `paciente_dir` text,
  `paciente_tlf` varchar(70) NOT NULL,
  `paciente_email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`paciente_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tratamientos`
--

CREATE TABLE IF NOT EXISTS `tratamientos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `odontodiagrama_id` int(11) NOT NULL,
  `observacion` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `odontodiagrama_id` (`odontodiagrama_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cedula` int(11) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `login` varchar(20) NOT NULL,
  `clave` varchar(50) NOT NULL,
  `confirmar` varchar(50) NOT NULL,
  `tipo` enum('Administrador','Médico','Asistente') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id`, `cedula`, `nombre`, `login`, `clave`, `confirmar`, `tipo`) VALUES
(1, 123456789, 'root', 'root', 'dc76e9f0c0006e8f919e0c515c66dbba3982f785', '', 'Administrador'),
(3, 42134231, 'Juan Gonzalez', 'jgonzalez', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Médico'),
(4, 42443232, 'Carlos Pérez', 'cperez', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Asistente');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
