<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sesmedico extends CI_Controller {
  
  private $sources;
  public $pacienteA;
  
  function __construct()
  {
    
    parent::__construct();
    
    $this->load->library('session');
    if (!$this->session->userdata("logged_in") && !$this->session->userdata("tipo")=="Médico"){
      redirect('/');
    }

    $this->sources = $this->constantes->assets();
    $this->pacienteA = null;

    $this->load->library('grocery_CRUD');
    $this->load->library('datemanager');
   
  }
  
  function llama_vista($vista = "sesmedico_vista", $output = null)
  {
    $this->sources['css_files'] = $output->css_files;
    $pac = $this->db->join('pacientes','pacientes.id = espera.pacientes_id')->where('espera.activo','Si')->get('espera');
    $this->sources['paciente'] = $pac->result();

    $this->load->view('cabeceramedico_vista', $this->sources);
    $this->load->view($vista, $output);
    $this->sources['js_files'] = $output->js_files;
    $this->load->view('piemedico_vista', $this->sources);
  }

  public function index($datos = null)
  {

    $pac = $this->db->join('pacientes','pacientes.id = espera.pacientes_id')->where('espera.activo','Si')->get('espera');
    $this->pacienteA = $pac->result();
    $datos['paciente'] = $this->pacienteA;
    
    $this->load->view('cabeceramedico_vista', $this->sources);
    $this->load->view('sesmedico_vista', $datos);
    $this->load->view('piemedico_vista', $this->sources);

  }

  public function consultas($datos = null)
  {
    $consultas = new grocery_CRUD();
    $consultas->set_table("consultas");
    $consultas->set_subject("Consultas");
    $consultas->set_theme("datatables");
    
    $consultas->order_by('id','DESC');
    $consultas->where('paciente_id',$this->uri->segment(3));
    $consultas->set_relation('paciente_id','pacientes','{cedula} - {nombres} {apellidos} Historia: {historia}');
    $consultas->set_relation('medico_id','medicos','{nombres}');
    $consultas->columns("fechaconsul","motivo", "diagnostico", "medico_id");

    $consultas->display_as('fechaconsul', 'Fecha');
    $consultas->display_as('diagnostico', 'Diagnóstico');
    $consultas->display_as('medico_id', 'Médico');

    $consultas->unset_add();
    $consultas->unset_edit();
    $consultas->unset_delete();

    $output = $consultas->render();

    $this->llama_vista("sesmedicos_consulta_vista",$output);
  
  }

  public function reposos($datos = null)
  {
    $reposos = new grocery_CRUD();
    $reposos->set_table("reposos");
    $reposos->set_subject("Reposo");
    $reposos->set_theme("datatables");
    $reposos->unset_texteditor('motivo');
    $reposos->unset_texteditor('diagnostico');
    $reposos->unset_texteditor('observaciones');
    $reposos->set_relation('pacientes_id','pacientes','{cedula} - {nombres} {apellidos} Historia: {historia}');
    $reposos->set_relation('medicos_id','medicos','{nombres}');
    $reposos->where('pacientes_id',$this->uri->segment(3));
    $reposos->columns("fechareposo", "motivo","desde", "hasta", "diagnostico", "observaciones", "pacientes_id", "medicos_id");

    // $r = $this->db->where('id',$this->uri->segment(3))->get('reposos')->result();
    
    // echo "<br><br><br><br><br>";
    // echo "<pre>";
    // print_r($r);
    // echo "</pre>";
    $p = $this->db->where('id',$this->uri->segment(3))->get('pacientes')->result();
    
    $reposos->field_type('pacientes_id','hidden',$this->uri->segment(3));
    $reposos->field_type('paciente_nombre','hidden',$p[0]->nombres." ".$p[0]->apellidos);


    $reposos->field_type('fechareposo','hidden',date('Y-m-d'));

    $m = $this->db->where('cedula',$this->session->userdata("cedula"))->get('medicos');
    $medico = $m->result();

    $reposos->field_type('medicos_id','hidden',$medico[0]->id);
    $reposos->field_type('medico_nombre','hidden',$medico[0]->nombres);
    
    $reposos->order_by('id','DESC');

    $reposos->display_as('fechareposo', 'Fecha');
    $reposos->display_as('pacientes_id', '# Paciente');
    $reposos->display_as('paciente_nombre', 'Nombre del Paciente');
    $reposos->display_as('medicos_id', '# Médico');
    $reposos->display_as('medico_nombre', 'Nombre del Médico');
    $reposos->display_as('diagnostico', 'Diagnóstico');
    // $reposos->callback_field('pacientes_id',array($this,'_callback_paciente'));
    
    $output = $reposos->render();

    $this->llama_vista("sesmedicos_reposo_vista",$output);
  
  }

  // public function _callback_webpage_url($value, $row)
  // {
  //   return "";
  // }

  public function guardar()
  {
    $this->load->library("form_validation");
    $this->form_validation->set_rules("recipe", "Récipe", "required|trim|min_length[5]");
    $this->form_validation->set_rules("indicaciones", "Indicaciones", "required|trim|min_length[10]");
    $this->form_validation->set_rules("diagnostico", "Diagnóstico", "required|trim|min_length[10]");
    $this->form_validation->set_rules("observaciones", "Observaciones", "min_length[10]");

    if ($this->form_validation->run() == FALSE) 
    {

      $this->index();

    } 
    else 
    {

      $condiciones = array('activo' => 'Si','fecha' => date('Y-m-d'));
      
      if($this->db->delete('espera',$condiciones))
      {
        $this->db->update('pacientes', array('activo' => 'No') );
        $siguienteEspera = $this->db->order_by('id','ASC')->where('fecha',date('Y-m-d'))->limit(1)->get('espera');
        $siguiente = $siguienteEspera->result();
        if (!empty($siguiente)) {
          $this->db->update('espera',array('activo'=>'Si'),array('id'=>$siguiente[0]->id));

          $this->db->update('pacientes', array('activo' => 'No') );
          $this->db->update('pacientes', array('activo' => 'Si') , array('id' => $siguiente[0]->pacientes_id) );
        }

        $m = $this->db->where('cedula',$this->input->post('medico_cedula'))->get('medicos');
        $medico = $m->result();

        $consulta = array(
                    'medico_id' => $medico[0]->id,
                    'fechaconsul' => $this->input->post('fechaconsul') ,
                    'paciente_id' => $this->input->post('paciente_id') , 
                    'motivo' => $this->input->post('motivo') , 
                    'recipe' => $this->input->post('recipe') , 
                    'indicaciones' => $this->input->post('indicaciones') , 
                    'diagnostico' => $this->input->post('diagnostico') , 
                    'observaciones' => $this->input->post('observaciones') , 
                    'glicemia' => $this->input->post('glicemia') , 
                    'urea' => $this->input->post('urea') , 
                    'orina' => $this->input->post('orina') , 
                    'heces' => $this->input->post('heces') , 
                    'hierro' => $this->input->post('hierro') , 
                    'calcio' => $this->input->post('calcio'),
                    'otros' => $this->input->post('otros') 
                    );
        $this->db->insert('consultas',$consulta);
        
        // imprimir recipe
        $pac = $this->db->where('id',$this->input->post('paciente_id'))->get('pacientes');
        $this->pacienteA = $pac->result();
        $datos['paciente'] = $this->pacienteA;
        $datos['medico'] = $medico;
        $datos['consulta'] = $consulta;
        
        $this->load->view('cabecerarecipe_vista', $this->sources);
        $this->load->view('recipe_vista', $datos);
        $this->load->view('pierecipe_vista', $this->sources);

      }

    }

  }

  public function informe()
  {
    
      $c = $this->db->where('paciente_id',$this->uri->segment(3))->limit(1)->order_by("id","DESC")->get('consultas');
      $consulta = $c->result();

      $pac = $this->db->where('id',$this->uri->segment(3))->get('pacientes');
      $paciente = $pac->result();

      $m = $this->db->join('especialidades','medicos.especialidad_id = especialidades.id')->where('cedula',$this->session->userdata('cedula'))->get('medicos');
      $medico = $m->result();

      // imprimir informe
      $datos['paciente'] = $paciente;
      $datos['medico'] = $medico;
      $datos['consulta'] = $consulta;
     
      $this->load->view('cabecerarecipe_vista', $this->sources);
      $this->load->view('informe_vista', $datos);
      $this->load->view('pierecipe_vista', $this->sources);

  }

  public function logout()
  {
  
    $this->session->set_userdata("logged_in", FALSE);
    $this->session->sess_destroy();
    redirect('/');
  
  }

}

