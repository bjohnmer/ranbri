<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends CI_Controller {
  
  private $sources;
  
  function __construct()
  {
    
    parent::__construct();
    
    $this->load->library('session');
    if (!$this->session->userdata("logged_in")  || !$this->session->userdata("tipo")=="Administrador"){
      redirect('/');
    }
    
    $this->sources = $this->constantes->assets();

    $this->load->library("grocery_CRUD");
   
  }
  
  public function mostrar($output = null, $vista = "lusuarios_vista")
  {
  
    $this->sources['css_files'] = $output->css_files;
    $this->load->view('cabeceraadmin_vista', $this->sources);
    $this->load->view($vista, $output);
    $this->sources['js_files'] = $output->js_files;
    $this->load->view('pieadmin_vista', $this->sources);

  }

  public function lusuarios()
  {
    
    $usuarios = new grocery_CRUD();
    $usuarios->set_table('usuarios');
    $usuarios->set_subject('Usuario');
    $usuarios->set_theme("datatables");
    
    $usuarios->display_as('nombre', 'Nombre');
    $usuarios->display_as('login_usuario', 'Login');
    $usuarios->display_as('clave', 'Clave');
    $usuarios->display_as('confirmar', 'Confirmación de Clave');
    $usuarios->display_as('tipo', 'Tipo de Usuario');
    
    $usuarios->columns("nombre","login","tipo");
    $usuarios->field_type('clave', 'password');
    $usuarios->field_type('confirmar', 'password');
    
    $usuarios->set_rules('nombre', 'Nombre del Usuario',"required|alpha_space|max_length[40]|min_length[2]");
    $usuarios->set_rules('login', 'Login',"required|alpha|max_length[20]|min_length[4]");
    $usuarios->set_rules('clave', 'Clave',"max_length[20]|min_length[3]|matches[confirmar]");
    $usuarios->set_rules('confirmar', 'Confirmación de Clave',"max_length[20]|min_length[3]|matches[clave]");
    $usuarios->set_rules('tipo', 'Tipo de Usuario',"required");

    $usuarios->callback_before_insert(array($this,'encrypt_clave_callback'));
    $usuarios->callback_before_update(array($this,'encrypt_confirmar_callback'));

    $usuarios->callback_edit_field('clave',array($this,'vaciar_campo_clave'));
    $usuarios->callback_edit_field('confirmar',array($this,'vaciar_campo_confirmar'));

    $output = $usuarios->render();
    $this->mostrar($output);
  
  }

  function encrypt_clave_callback($post_array, $primary_key = null)
  {
      
      $post_array['clave'] = sha1($post_array['clave']);
      $post_array['confirmar'] = sha1($post_array['confirmar']);

      return $post_array;
  }

  function encrypt_confirmar_callback($post_array, $primary_key = null)
  {
      
      if (!empty($post_array['clave']))
      {
          
          $post_array['clave'] = sha1($post_array['clave']);
          $post_array['confirmar'] = sha1($post_array['confirmar']);
      
      }
      else
      {
      
          unset($post_array['clave']);
          unset($post_array['confirmar']);
      
      }
      
      return $post_array;
  
  }

  function vaciar_campo_clave($post_array, $primary_key = null)
  {
      
      return '<input type="password" maxlength="50" name="clave" id="field-clave">';
  
  }

  function vaciar_campo_confirmar($post_array, $primary_key = null)
  {
      
      return '<input type="password" maxlength="50" name="confirmar" id="field-confirmar">';
  
  }

  public function nusuario()
  {
    
    redirect("usuarios/lusuarios/add");
  
  }

  public function atras()
  {
    
    redirect("admin");
  
  }

}
