<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pacientes extends CI_Controller {
  
  private $sources;
  
  function __construct()
  {
    
    parent::__construct();
    
    $this->load->library('session');
    if (!$this->session->userdata("logged_in")  || !$this->session->userdata("tipo")=="Administrador" || !$this->session->userdata("tipo")=="Asistente"){
      redirect('/');
    }
    
    $this->sources = $this->constantes->assets();

    $this->load->library("grocery_CRUD");
   
  }
  
  public function mostrar($output = null, $vista = "lpacientes_vista")
  {
  
    $this->sources['css_files'] = $output->css_files;
    $this->load->view('cabeceraadmin_vista', $this->sources);
    $this->load->view($vista, $output);
    $this->sources['js_files'] = $output->js_files;
    $this->load->view('pieadmin_vista', $this->sources);

  }

  public function lpacientes()
  {
    
    $pacientes = new grocery_CRUD();
    $pacientes->set_table("pacientes");
    $pacientes->set_subject("paciente");
    $pacientes->set_theme("datatables");
    $pacientes->columns("paciente_ci","cedupaciente_nombrecompletola","paciente_fnac","paciente_tlf");
    $pacientes->unset_texteditor(array('paciente_dir'));
    
    $pacientes->display_as('paciente_nombrecompleto','Nombre Completo');
    $pacientes->display_as('paciente_ci','Cédula');
    $pacientes->display_as('paciente_edad','Edad');
    $pacientes->display_as('paciente_fnac','Fecha de Naciemiento');
    $pacientes->display_as('paciente_sexo','Sexo');
    $pacientes->display_as('paciente_dir','Dirección');
    $pacientes->display_as('paciente_tlf','Teléfonos');
    $pacientes->display_as('paciente_email','Correo Electrónico');

    $pacientes->set_rules('paciente_nombrecompleto','Nombre Completo','required|alpha_space|min_length[2]|max_length[100]');
    $pacientes->set_rules('paciente_ci','Cédula', 'required|numeric|min_length[2]|max_length[12]');
    $pacientes->set_rules('paciente_edad','Edad', 'numeric');
    $pacientes->set_rules('paciente_dir','Dirección','min_length[2]');
    $pacientes->set_rules('paciente_tlf','Teléfonos','required|min_length[6]');
    $pacientes->set_rules('paciente_email','Correo Electrónico','valid_email|is_unique[pacientes.paciente_email]');
    $pacientes->set_rules('paciente_sexo',"Sexo",'required');

    $output = $pacientes->render();
    $this->mostrar($output);
  
  }

  public function historias()
  {
    
    $historias = new grocery_CRUD();
    $historias->set_table("historias");
    $historias->set_subject("Historia");
    $historias->set_theme("datatables");
    $historias->columns("historia_num","paciente_id","historia_fecha");
    
    $historias->display_as('paciente_id','Paciente');
    $historias->display_as('historia_mc','Motivo de Consulta');
    $historias->display_as('historia_tp','Tratamiento Previo');
    $historias->display_as('historia_alergico','Alérgico');
    $historias->display_as('historia_habitos','Hábitos');
    $historias->display_as('historia_am','Antecedentes Médicos');
    $historias->display_as('historia_soportes','Soportes');
    $historias->display_as('historia_maxsup','Maxilar Superior');
    $historias->display_as('historia_maxinf','Maxilar Inferior');
    $historias->display_as('historia_oclusion','Oclusión');
    $historias->display_as('historia_fecha','Fecha');

    $historias->set_relation('paciente_id','pacientes','{paciente_ci} - {paciente_nombrecompleto}');
    $historias->set_relation('paciente_id','pacientes','{paciente_ci} - {paciente_nombrecompleto}');
    $historias->unset_texteditor(array('historia_mc','historia_tp','historia_alergico','historia_habitos','historia_soportes','historia_maxsup','historia_maxinf','historia_oclusion','historia_am'));

    $output = $historias->render();
    $this->mostrar($output,'lhistorias_vista');
  
  }

  public function consultas()
  {
    $consultas = new grocery_CRUD();
    $consultas->set_table("consultas");
    $consultas->set_subject("Consulta");
    $consultas->set_relation('medico_id','medicos','{cedula} - {nombres}');
    $consultas->set_relation('paciente_id','pacientes','{cedula} - {nombres} {apellidos}');
    $consultas->set_theme("datatables");
    $consultas->columns('fechaconsul','paciente_id','medico_id','motivo');
    $consultas->display_as('fechaconsul','Fecha de Consulta');
    $consultas->display_as('diagnostico','Diagnóstico');
    $consultas->display_as('medico_id','Médico');
    $consultas->display_as('paciente_id','Paciente');
    $consultas->unset_add();
    $consultas->unset_delete();
    $consultas->unset_edit();
    $output = $consultas->render();
    $this->mostrar($output,'lconsultas_vista');
  }
  
  public function reposos()
  {
    $reposos = new grocery_CRUD();
    $reposos->set_table("reposos");
    $reposos->set_subject("Reposo");
    $reposos->set_relation('medicos_id','medicos','{cedula} - {nombres}');
    $reposos->set_relation('paciente_id','pacientes','{cedula} - {nombres} {apellidos}');
    $reposos->set_theme("datatables");
    $reposos->columns('fechareposo','paciente_id','medico_id','motivo', 'desde','hasta');
    $reposos->display_as('fechareposo','Fecha de Consulta');
    $reposos->display_as('medico_id','Médico');
    $reposos->display_as('paciente_id','Paciente');
    $reposos->unset_add();
    $reposos->unset_delete();
    $reposos->unset_edit();
    $output = $reposos->render();
    $this->mostrar($output,'lreposos_vista');
  }
  public function npaciente()
  {
    
    redirect("pacientes/lpacientes/add");
  
  }

  public function atras()
  {
    
    redirect("admin");
  
  }

}
