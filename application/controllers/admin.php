<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
  
  private $sources;
  
  function __construct()
  {
    
    parent::__construct();
    
    $this->load->library('session');
    if (!$this->session->userdata("logged_in") || !$this->session->userdata("tipo")=="Administrador" || !$this->session->userdata("tipo")=="Asistente"){
      redirect('/');
    }

    $this->sources = $this->constantes->assets();
   
  }
  
  public function index($datos = null, $vista = "admin_vista")
  {

    $this->load->view('cabeceraadmin_vista', $this->sources);
    $this->load->view($vista, $datos);
    $this->load->view('pieadmin_vista', $this->sources);

  }

  public function medicos($datos = null)
  {
    
    $this->index($datos,"medicos_vista");
  
  }

  public function pacientes($datos = null)
  {
    
    $this->index($datos,"pacientes_vista");
  
  }

  public function usuarios($datos = null)
  {
    
    $this->index($datos,"usuarios_vista");
  
  }

  public function espera($datos = null)
  {
    
    // $this->index($datos,"espera_vista");
    redirect('espera');
  
  }

  public function logout()
  {
  
    $this->session->set_userdata("logged_in", FALSE);
    $this->session->sess_destroy();
    redirect('/');
  
  }

}

