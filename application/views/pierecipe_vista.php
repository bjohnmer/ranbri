    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <p>&copy; RANBRI <?=date('Y')?></p>
        </div>
        <div class="col-lg-6">
          <!-- <p class="text-right">Diseño y Desarrollo: <strong>María Añez</strong></p> -->
        </div>
      </div>
    </div>
  </body>
  <?php if (!empty($js)): ?>
    <?php foreach ($js as $item): ?>
  <script src="<?=ASSETS_DIR?><?=$item?>"></script>
    <?php endforeach ?>
  <?php endif ?>
  <script>
    $(document).ready(function(){
      $("#imprimir").click(function(){
        $(".btn").hide();
        window.print();
        $(".btn").show();
      });
    });
  </script>
</html>
