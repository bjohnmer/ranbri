<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Maria Añez">

    <title>..:: RANBRI ::..</title>

    <?php if (!empty($css)): ?>
      <?php foreach ($css as $item): ?>
    <link href="<?=ASSETS_DIR?><?=$item?>" rel="stylesheet">
      <?php endforeach ?>
    <?php endif ?>

    <?php if (!empty($css_files)): ?>
    <?php foreach($css_files as $file): ?>
      <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>
    <?php endif ?>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?=ASSETS_DIR?>js/html5shiv.js"></script>
      <script src="<?=ASSETS_DIR?>js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
