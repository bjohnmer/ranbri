      <div class="clearfix separador"></div>

      <div class="footer">
        <div class="row">
          <div class="col-lg-6">
            <p>&copy; RANBRI <?=date('Y')?></p>
          </div>
          <div class="col-lg-6">
            <!-- <p class="text-right">Diseño y Desarrollo: <strong>María Añez</strong></p> -->
          </div>
        </div>
      </div>

    </div> <!-- /container -->
    
    
    <?php if (!empty($js)): ?>
      <?php foreach ($js as $item): ?>
    <script src="<?=ASSETS_DIR?><?=$item?>"></script>
      <?php endforeach ?>
    <?php endif ?>

    <?php if (!empty($js_files)): ?>
      <?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
      <?php endforeach; ?>
    <?php endif ?>
    
  </body>
</html>
