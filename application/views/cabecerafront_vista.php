<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="María Añez">

    <title>..:: RANBRI ::..</title>

    <?php if (!empty($css)): ?>
      <?php foreach ($css as $item): ?>
    <link href="<?=ASSETS_DIR?><?=$item?>" rel="stylesheet">
      <?php endforeach ?>
    <?php endif ?>
    
  </head>

  <body>
    
    <div class="container-narrow">
      <div class="header">
        <div class="row">
          <div class="col-lg-12">
            <img class="img-responsive" src="<?=ASSETS_DIR?>img/header.png" alt="DEM">
          </div>
        </div>
        <!--h3 class="text-muted text-center">Unidad de Servicios Médicos</h3-->
      </div>