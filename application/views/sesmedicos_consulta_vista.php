      <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Unidad de Servicios Médicos</a>
          </div>

          <div class="navbar-collapse collapse">
          
            <div class="navbar-form navbar-right">
                <?=$this->session->userdata('nombre')?> | <?=$this->session->userdata('tipo')?> <a href="<?=base_url()?>admin/logout" class="btn btn-danger">Cerrar Sesión</a>
            </div>

          </div><!--/.navbar-collapse -->
        </div>
      </div>
      
      <div class="clearfix separador"></div>

      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <h2>&nbsp;</h2>
            <h2>Datos del Paciente</h2>
            <h4><?=$paciente[0]->nombres?> <?=$paciente[0]->apellidos?></h4>
          </div>
          <div class="col-lg-6">
            <h2><?=date('d/m/Y')?></h2>
            <h2>Historia #: <?=$paciente[0]->historia?></h2>
            <h4>Cédula:<?=$paciente[0]->cedula?></h4>
          </div>
        </div>
      
      <h2 class="text-center">CONSULTAS</h2>
      <a href="<?=base_url()?>sesmedico" class="btn btn-success"><span class="glyphicon glyphicon-circle-arrow-left"> Atrás</a>
      <div class="row">
        <?=$output?>
      </div>

      </div>
    
      <div class="clearfix separador"></div>