
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><strong><?=date('d/m/Y')?></strong>&nbsp;&nbsp;&nbsp;Unidad de Servicios Médicos</a>
        </div>

        <div class="navbar-collapse collapse">
        
          <div class="navbar-form navbar-right">
              <?=$this->session->userdata('nombre')?> | <?=$this->session->userdata('tipo')?> <a href="<?=base_url()?>admin/logout" class="btn btn-danger">Cerrar Sesión</a>
          </div>

        </div><!--/.navbar-collapse -->
      </div>
    </div>
    
    <div class="clearfix separador"></div>

    <div class="container">
      <?php if (!empty($paciente)): ?>
        
      <?php if ($paciente[0]->emergencia == 'Si'): ?>
        <div class="row">
          <div class="col-lg-12 alert alert-danger">
            <p>¡Emergencia!</p>
          </div>
        </div>
      <?php endif ?>

      <?php if (!empty($mensaje)): ?>
      <div class="row">
        <div class="col-lg-12 alert alert-danger">
          <h5>Se han encontrado errores</h5>
          <p><?php echo $mensaje['mensaje']; ?></p>
        </div>
      </div>
      <?php endif ?>

      <div class="row">
        <div class="col-lg-6">
          <h2>Datos del Paciente</h2>
          <h4><?=$paciente[0]->nombres?> <?=$paciente[0]->apellidos?></h4>
          <p><a class="btn btn-success" href="<?=base_url()?>sesmedico/consultas/<?=$paciente[0]->id?>">Ver Consultas &raquo;</a></p>
        </div>
        <div class="col-lg-6">
          <h2>Historia #: <?=$paciente[0]->historia?></h2>
          <h4>Cédula:<?=$paciente[0]->cedula?></h4>
          <p><a class="btn btn-primary" href="<?=base_url()?>sesmedico/reposos/<?=$paciente[0]->id?>">Reposos &raquo;</a></p>
        </div>
      </div>
      <form role="form" action="<?=base_url()?>sesmedico/guardar" method="post">
        <input type="hidden" name="medico_cedula" value="<?=$this->session->userdata("cedula")?>">
        <input type="hidden" name="fechaconsul" value="<?=date("Y-m-d")?>">
        <input type="hidden" name="paciente_id" value="<?=$paciente[0]->pacientes_id?>">
        <div class="row">
          <div class="col-lg-12">
            <h4>Motivo</h4>
            <p><?=$paciente[0]->observaciones?></p>
            <input type="hidden" name="motivo" value="<?=$paciente[0]->observaciones?>">
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-lg-6">
            <h2>Récipe</h2>
            <p>
              <div class="form-group">
                <textarea class="form-control" name="recipe" id="recipe" rows="5"></textarea>
                
                <?php if (form_error('recipe')): ?>
                <div id="report-error" class="alert alert-block alert-danger fade in" style="display:block;">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <?=form_error('recipe')?>
                </div>
                <?php endif ?>

              </div>
            </p>
          </div>
          <div class="col-lg-6">
            <h2>Indicaciones</h2>
            <p>
              <div class="form-group">
                <textarea class="form-control" name="indicaciones" id="indicaciones" rows="5"></textarea>
                
                <?php if (form_error('indicaciones')): ?>
                <div id="report-error" class="alert alert-block alert-danger fade in" style="display:block;">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <?=form_error('indicaciones')?>
                </div>
                <?php endif ?>

              </div>
            </p>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6">
            <h2>Diagnóstico</h2>
            <p class="text-muted">&nbsp;</p>

            <p>
              <div class="form-group">
                <textarea class="form-control" name="diagnostico" id="diagnostico" rows="5"></textarea>
                <?php if (form_error('diagnostico')): ?>
                <div id="report-error" class="alert alert-block alert-danger fade in" style="display:block;">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <?=form_error('diagnostico')?>
                </div>
                <?php endif ?>
              </div>
            </p>
          </div>
          <div class="col-lg-6">
            <h2>Observaciones</h2>
            <p class="text-muted">Usted puede escribir aquí el texto que aparecerá en el Informe Médico</p>
            <p>
              <div class="form-group">
                <textarea class="form-control" name="observaciones" id="observaciones" rows="5"></textarea>
                <?php if (form_error('observaciones')): ?>
                <div id="report-error" class="alert alert-block alert-danger fade in" style="display:block;">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <?=form_error('observaciones')?>
                </div>
                <?php endif ?>
              </div>
            </p>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12">
            <h2>Exámenes</h2>
          </div>
          <div class="col-lg-2">
            <div class="checkbox">
              <label>
                <input type="checkbox" name="glicemia" value="Si"> Glicemia
              </label>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="creatinina" value="Si"> Creatinina
              </label>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="urea" value="Si"> Úrea
              </label>
            </div>
          </div>
          <div class="col-lg-2">
            <div class="checkbox">
              <label>
                <input type="checkbox" name="trigliceridos" value="Si"> Triglicéridos
              </label>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="colesterol" value="Si"> Colesterol
              </label>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="tsh" value="Si"> TSH
              </label>
            </div>
          </div>
          <div class="col-lg-2">
            <div class="checkbox">
              <label>
                <input type="checkbox" name="t3" value="Si"> T3
              </label>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="t4" value="Si"> T4
              </label>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="orina" value="Si"> Orina
              </label>
            </div>
          </div>
          <div class="col-lg-2">
            <div class="checkbox">
              <label>
                <input type="checkbox" name="heces" value="Si"> Heces
              </label>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="glicemia" value="Si"> Glicemia
              </label>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="tgo" value="Si"> TGO
              </label>
            </div>
          </div>
          <div class="col-lg-2">
            <div class="checkbox">
              <label>
                <input type="checkbox" name="tgp" value="Si"> TGP
              </label>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="hierro" value="Si"> Hierro
              </label>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="calcio" value="Si"> Calcio
              </label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <h3>Otros Exámenes</h3>
          </div>
          <div class="col-lg-12">
            <textarea class="form-control" type="text" name="otros" id="otros" rows="4"></textarea>
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-lg-12">
            <div class="form-group">
              <input type="submit" class="btn btn-success" value="Guardar e Imprimir">
              <input type="reset" class="btn btn-danger" value="Cancelar">
            </div>
          </div>
        </div>

      </form>
      <?php else: ?>
        <h1>No hay pacientes en la lista</h1>
        <a href="<?=base_url()?>sesmedico" class="btn btn-success"><span class="glyphicon glyphicon-refresh">&nbsp;Actualizar Pantalla</a>
      <?php endif ?>

    </div>
