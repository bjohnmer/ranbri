<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="María Añez">

    <title>..:: RANBRI ::..</title>

    <?php if (!empty($css)): ?>
      <?php foreach ($css as $item): ?>
    <link href="<?=ASSETS_DIR?><?=$item?>" rel="stylesheet">
      <?php endforeach ?>
    <?php endif ?>

    <?php if (!empty($css_files)): ?>
    <?php foreach($css_files as $file): ?>
      <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>
    <?php endif ?>

  </head>

  <body>

    <div class="container">
      <div class="header">
        <div class="row">
          <div class="col-lg-12">
            <a href="index.html">
              <img class="img-responsive" src="<?=ASSETS_DIR?>img/headeradmin.png" alt="DEM">
            </a>
          </div>
        </div>
        
        <!--h3 class="text-muted text-center">Unidad de Servicios Médicos</h3-->
      </div>
      <div class="row">
        <p class="text-right">Nombre de Usuario <a href="<?=base_url()?>admin/logout" class="btn btn-mini btn-default">Cerrar Sesión</a></p>
      </div>
      <div class="row">
        <h3 class="text-center">ÁREA ADMINISTRATIVA</h3>
      </div>