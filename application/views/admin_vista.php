
      <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-4 text-center">&nbsp;&nbsp;&nbsp;<a href="<?=base_url()?>historias" class="btn btn-danger">Historias</a></div>
        <div class="col-lg-4"></div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-lg-4 text-right box-menu"><a href="<?=base_url()?>admin/pacientes" class="btn btn-danger">Pacientes</a></div>
        <div class="col-lg-4">
          <img src="<?=ASSETS_DIR?>img/medicina.jpg" alt="Medicina" class="img-responsive img-menu">
        </div>
        <?php if ($this->session->userdata("tipo")=="Administrador"): ?>
          <div class="col-lg-4 box-menu"><a href="<?=base_url()?>admin/medicos" class="btn btn-danger">Médicos</a></div>
        <?php endif ?>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-lg-4"></div>
        <?php if ($this->session->userdata("tipo")=="Administrador"): ?>
        <div class="col-lg-4 text-center">&nbsp;&nbsp;&nbsp;<a href="<?=base_url()?>admin/usuarios" class="btn btn-danger">Usuarios</a></div>
        <?php endif ?>
        <div class="col-lg-4"></div>
      </div>
      <div class="clearfix separador"></div>
