      <div class="row">
        <h3 class="text-center">INFORMACIÓN DE USUARIOS</h3>
      </div>
      <div class="row">
        <div class="col-lg-4">
          <div class="btn-group-vertical">
            <a href="<?=base_url()?>usuarios/nusuario" class="btn btn-danger">Nuevo Usuario</a><br>
            <a href="<?=base_url()?>usuarios/lusuarios" class="btn btn-danger">Lista de Usuarios</a><br>
            <a href="<?=base_url()?>usuarios/atras" class="btn btn-success"><span class="glyphicon glyphicon-circle-arrow-left"> Atrás</a><br>
          </div>
        </div>
        <div class="col-lg-8">
          <img src="<?=ASSETS_DIR?>img/usuarios.gif" alt="" class="img-responsive img-menu">
        </div>
        <div class="col-lg-4 text-center">&nbsp;&nbsp;&nbsp;</div>
        <div class="col-lg-4"></div>
      </div>
      