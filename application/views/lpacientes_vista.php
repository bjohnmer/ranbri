      <div class="row">
        <div class="col-lg-2 col-offset-5">
          <img src="<?=ASSETS_DIR?>img/pacientes.jpg" alt="" class="pull-center img-responsive">
        </div>
      </div>
      <h2 class="text-center">PACIENTES</h2>
      <a href="<?=base_url()?>admin/pacientes" class="btn btn-success"><span class="glyphicon glyphicon-circle-arrow-left"> Atrás</a>
      <div class="row">
        <?=$output?>
      </div>
      <div class="clearfix separador"></div>

    