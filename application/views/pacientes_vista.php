
      <div class="row">
        <h3 class="text-center">INFORMACIÓN DE PACIENTES</h3>
      </div>
      <div class="row">
        <div class="col-lg-4">
        <div class="btn-group-vertical">
          <!-- <a href="<?=base_url()?>pacientes/reposos" class="btn btn-danger">Reposos Médicos</a><br> -->
          <a href="<?=base_url()?>pacientes/npaciente" class="btn btn-danger">Nuevo Paciente</a><br>
          <a href="<?=base_url()?>pacientes/lpacientes" class="btn btn-danger">Lista de Pacientes</a><br>
          <a href="<?=base_url()?>pacientes/historias" class="btn btn-danger">Historias</a><br>
          <a href="<?=base_url()?>pacientes/atras" class="btn btn-success"><span class="glyphicon glyphicon-circle-arrow-left"> Atrás</a><br>
        </div>

        </div>
        <div class="col-lg-8">
          <img src="<?=ASSETS_DIR?>img/pacientes.jpg" alt="" class="img-responsive img-menu">
        </div>
        <div class="col-lg-4 text-center">&nbsp;&nbsp;&nbsp;</div>
        <div class="col-lg-4"></div>
      </div>
      