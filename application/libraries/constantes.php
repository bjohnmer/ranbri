<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Constantes
*/
define('ASSETS_DIR', base_url()."assets/");

class Constantes{
  
  public function assets()
  {
    return array(
      'css' => array(
          0 => 'css/bootstrap.css',
          1 => 'css/jumbotron-narrow.css',
          2 => 'css/estilos.css',
          3 => 'css/bootstrap-glyphicons.css'
        ),
      'js'  => array(
          0 => 'js/jquery.js',
          1 => 'js/bootstrap.min.js',
          2 => 'js/respond.js'
        ), 
      );
  }

}